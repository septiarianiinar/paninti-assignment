# Paninti Assignment

In this assignment, the backend is already setup and you will just need to deliver the UI portion of it by consuming the APIs. Please build this project with Swift. You can use MVC, MVVM or other design architecture to build this project.

Your project should utilise all the APIs and we will evaluate your project based on the following project criteria:
1. Project runs as expected without crashing
2. User is able to view the list of leagues (choose min. 3 leagues)
3. User is able to view the list of teams in group by league (List View & Grid View)
4. User is able to view the detail of each team (Use data from selected team from the list)
5. User is able to add team to favorite on their own devices (online/offline)
6. User is able to see list of their favorite teams eventhough without connected to network (offline)
7. User is able to delete one or many teams (bulk delete) from their favorite

Please feel free to create the UI and we can walkthrough your ideas during your follow up interview.

**You are free to use any other libraries that you feel is needed for this project. But we are hoping candidate DO NOT heavily rely on UI library.**

## APIs Usage
For the documentation of the API, you can get it from https://www.thesportsdb.com/api.php

1. The base url is https://www.thesportsdb.com/api/v1/json
2. Get all the leagues using "/3/all_leagues.php" (API List all leagues)
3. Get all the teams of leagues via (API List all Teams in a League) "/3/search_all_teams.php?l=" with league’s name you get from API Get list of leagues (strLeague)

For the local data you could use Realm or UserDefaults.
Note : Please set minimum Xcode version to 13.4.1

Push your source code to gitlab and please submit the repo link before Friday, February 24th 2023 at 10:00 WIB.

**Have fun with the project!**
**GOOD LUCK!**
